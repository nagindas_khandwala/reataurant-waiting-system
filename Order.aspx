﻿<%@ Page Title="" Language="C#" MasterPageFile="~/layout.master" AutoEventWireup="true" CodeFile="Order.aspx.cs" Inherits="Order" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
     <style type="text/css">
        .auto-style3 {
            width: 319px;
            height: 68px;
        }
        .auto-style4 {
            width: 398px;
            height: 68px;
        }
        .auto-style5 {
            width: 319px;
            height: 75px;
        }
        .auto-style6 {
            width: 398px;
            height: 75px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <div>
       
        <table style="width: 383px; height: 299px; margin-left: 397px; margin-top: 62px">
           <tr><td style="text-align:center" colspan="2"><h2>Please Confirm it..</h2></td></tr>
            
          

            <tr>
                <td class="auto-style3">
                    <h3>Table No. :</h3></td>
                <td class="auto-style4">
                    <asp:Label ID="Lbltable" runat="server" Text=""></asp:Label></td>
            </tr>

            <tr>
                <td class="auto-style3">
                    <h3>Password:</h3></td>
                <td class="auto-style4">
                    <asp:TextBox ID="txtpaswd" runat="server" TextMode="Password"></asp:TextBox></td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Please Enter Password" ForeColor="#CC0000" ControlToValidate="txtpaswd"></asp:RequiredFieldValidator></td>
            </tr>
            <tr>
                <td colspan="2" style="text-align:center">
                    <asp:Button ID="Proceed" runat="server" Text="Proceed" Width="121px" Height="36px" Onclick="Proceed_Click"/></td>
            </tr>

             <tr><td colspan="2" style="text-align:center">
                <asp:Label ID="LblMsg" runat="server" ForeColor="Red"></asp:Label></td></tr>
        </table>
    </div>
</asp:Content>

