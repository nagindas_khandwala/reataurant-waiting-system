﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Main.aspx.cs" Inherits="Main" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="css/homess.css" rel="stylesheet" />
    <link href="css/navigation.css" rel="stylesheet" />
    <style type="text/css">
        .auto-style1 {
            width: 298px;
        }

        .auto-style2 {
            width: 102px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <div id="wrapper">

                <div id="banner">
                </div>
                <div id="navigation">
                    <ul id="nav">
                        <li><a href="Main.aspx"><b>Menu Card</b></a></li>
                        <li><a href="#"><b>Give Feedback</b></a></li>
                        <li><a href="#"><b>View State</b></a></li>
                    </ul>
                </div>
                <div id="content_area">
                    <table style="height: 408px; width: 1022px">
                        <tr>
                            <td class="auto-style1">
                           <h2>Welcome... <asp:Label ID="Lblname" runat="server" Text=""></asp:Label></h2>
                                <table>
                                    <tr>
                                        <td class="auto-style2" style="height:40px;width:100px">
                                            <h3>Category: </h3>
                                        </td>
                                        <td class="auto-style6" style="height:40px;width:100px">
                                            <asp:DropDownList ID="ddlmenu" runat="server">
                                                <asp:ListItem>Starter</asp:ListItem>
                                                <asp:ListItem>Main Course</asp:ListItem>
                                                <asp:ListItem>Desert</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                    </tr>

                                    <tr>
                                        <td colspan="2" style="text-align: center">
                                            <asp:Button ID="Button1" runat="server" Text="Ok" Width="121px" Height="36px" Onclick="Button1_Click" />

                                        </td>
                                    </tr>
                                </table>
                            </td>

                            <td>
                                <asp:GridView ID="Grdmenu" runat="server" BackColor="LightGoldenrodYellow" BorderColor="Tan" BorderWidth="1px" CellPadding="2" ForeColor="Black" GridLines="None" OnSelectedIndexChanged="Grdmenu_SelectedIndexChanged" style="margin-left: 78px" Height="274px" Width="568px" >
                                    <AlternatingRowStyle BackColor="PaleGoldenrod" />
                                    <FooterStyle BackColor="Tan" />
                                    <HeaderStyle BackColor="Tan" Font-Bold="True" />
                                    <PagerStyle BackColor="PaleGoldenrod" ForeColor="DarkSlateBlue" HorizontalAlign="Center" />
                                    <SelectedRowStyle BackColor="DarkSlateBlue" ForeColor="GhostWhite" />
                                    <SortedAscendingCellStyle BackColor="#FAFAE7" />
                                    <SortedAscendingHeaderStyle BackColor="#DAC09E" />
                                    <SortedDescendingCellStyle BackColor="#E1DB9C" />
                                    <SortedDescendingHeaderStyle BackColor="#C2A47B" />
                                    <Columns>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="Chkbox" runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                                    <pre>
                                        <asp:Button ID="Button2" runat="server" Text="Button"></asp:Button>
                                    
                                    </pre>
                           </td>
                        </tr> 
                     </table>

                </div>
                <div id="footer">
                    All rights reserved..!!!
                </div>
    </form>
</body>
</html>
