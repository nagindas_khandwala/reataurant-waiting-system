﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class chef : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void Button1_Click(object sender, EventArgs e)
    {
                 SqlConnection conn = null;
            SqlCommand cmd = null;
            try
            {
                conn = new SqlConnection(ConfigurationManager.ConnectionStrings["payal"].ConnectionString);
                if(conn.State==ConnectionState.Closed)
                {
                    conn.Open();
                }
                string query = "select count(*) from Login where Username=@username and Password=@password";
                cmd = new SqlCommand(query,conn);
                cmd.Parameters.AddWithValue("@username",txtname.Text);
                cmd.Parameters.AddWithValue("@password", txtpasswd.Text);
                int count = Convert.ToInt32(cmd.ExecuteScalar());
                if(count==1)
                {
                    Session["username"] = txtname.Text;
                    Response.Redirect("kitchen.aspx");
                    
                }
                else
                {
                    LblMsg.Text = "Invalid username and password";
                   
                }
                   
            }
            catch (Exception)
            {
                
                throw;
            }
        }
    }
