﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Order : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Session["tableno"] != null)
            {
                Lbltable.Text = Session["tableno"].ToString();
            }
        }
    }
    protected void Proceed_Click(object sender, EventArgs e)
    {
        SqlConnection conn = null;
            SqlCommand cmd = null;
            try
            {
                conn = new SqlConnection(ConfigurationManager.ConnectionStrings["payal"].ConnectionString);
                if(conn.State==ConnectionState.Closed)
                {
                    conn.Open();
                }
                string query = "select count(*) from Waiter where password=@password";
                cmd = new SqlCommand(query,conn);
                cmd.Parameters.AddWithValue("@password", txtpaswd.Text);
                int count = Convert.ToInt32(cmd.ExecuteScalar());
                if(count==1)
                {
                    Response.Redirect("Main.aspx");
                    
                }
                else
                {
                    LblMsg.Text = "Invalid password";
                   
                }
                   
            }
            catch (Exception)
            {
                
                throw;
            }
        }
    }