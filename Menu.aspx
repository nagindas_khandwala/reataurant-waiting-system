﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Menu.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
   
    <link href="css/navigation.css" rel="stylesheet" />
   <link href="css/homess.css" rel="stylesheet" />
   
</head>
<body>
    <form id="form1" runat="server">
        <div id="wrapper">
        <div id="banner">

        </div>
        <div id="navigation">
                    <ul>
                         <li class="dropdown">
                   <b> <a class="dropbtn">Menu Items</a></b>
                    <div class="dropdown-content">
                      <b><a href="additem.aspx">Add items</a></b>
                      <b><a href="viewmenu.aspx">View Menu Card</a></b>
             
                    </div>
                </li>
                        <li><a href="Menu.aspx"><b>View Profile</b></a></li>
                        <li class="dropdown">
                   <b> <a class="dropbtn">Chef Details</a></b>
                    <div class="dropdown-content">
                      <b><a href="rmvchef.aspx">Remove Chef</a></b>
                      <b><a href="addchef.aspx">Add Chef</a></b>
                      <b><a href="viewchef.aspx">View Chef</a></b>
                    </div>
                </li>
                        <li><a href="#"><b>View Feedback</b></a></li>
                        <li><a href="#"><b>Log Out</b></a></li>
                    </ul>
                </div>

        <div id="content_area">
            

        </div>

        <div id="footer">
            All rights reserved..!!!
        </div>
            </div>
    </form>
</body>
</html>
