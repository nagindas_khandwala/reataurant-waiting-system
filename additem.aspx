﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="additem.aspx.cs" Inherits="additem" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
     <link href="css/navigation.css" rel="stylesheet" />
   <link href="css/homess.css" rel="stylesheet" />2
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <div id="wrapper">
        <div id="banner">

        </div>
        <div id="navigation">
                    <ul>
                         <li class="dropdown">
                   <b> <a class="dropbtn">Menu Items</a></b>
                    <div class="dropdown-content">
                      <b><a href="additems.aspx">Add items</a></b>
                      <b><a href="viewmenu.aspx">View Menu Card</a></b>
             
                    </div>
                </li>
                        <li><a href="Menu.aspx"><b>View Profile</b></a></li>
                        <li class="dropdown">
                   <b> <a class="dropbtn">Chef Details</a></b>
                    <div class="dropdown-content">
                      <b><a href="rmvchef.aspx">Remove Chef</a></b>
                      <b><a href="addchef.aspx">Add Chef</a></b>
                      <b><a href="viewchef.aspx">View Chef</a></b>
                    </div>
                </li>
                        <li><a href="#"><b>View Feedback</b></a></li>
                        <li><a href="#"><b>Log Out</b></a></li>
                    </ul>
                </div>

        <div id="content_area">
            <table style="height: 408px; width: 1022px">
                <tr>
                    <td class="auto-style1">
                        <table>
                                                   <tr>
                <td class="auto-style2">
                    <h3>Menu Name :</h3></td>
                <td class="auto-style6">
                    <asp:TextBox ID="txtmenuname" runat="server" Height="28px" Width="160px"></asp:TextBox></td>
            </tr>

            <tr>
                <td class="auto-style2">
                    <h3>Price :</h3></td>
                <td class="auto-style4">
                    <asp:TextBox ID="txtprice" runat="server" Height="28px" Width="160px"></asp:TextBox></td>
            </tr>

             <tr>
                <td class="auto-style2">
                    <h3>Category:</h3></td>
                <td class="auto-style4">
                    <asp:TextBox ID="txtcat" runat="server" Height="28px" Width="160px"></asp:TextBox></td>
            </tr>   

            <tr>
                <td colspan="2" style="text-align:center">
                    <asp:Button ID="btnadd" runat="server" Text="ADD" Width="121px" Height="36px" OnClick="btnadd_Click" /></td>
            </tr>
                        </table>
                        <asp:Label ID="Lblmsg" runat="server" Text=""></asp:Label>
                    </td>

                    <td>
                        <asp:GridView ID="GridView1" runat="server" BackColor="LightGoldenrodYellow" BorderColor="Tan" BorderWidth="1px" CellPadding="2" ForeColor="Black" GridLines="None" Height="295px" style="margin-left: 53px; margin-top: 0px" Width="648px" OnRowDeleting="GridView1_RowDeleting" OnSelectedIndexChanged="GridView1_SelectedIndexChanged" DataKeyNames="menuid">
                           <Columns>
                                <asp:CommandField HeaderText="Edit" ShowSelectButton="true" SelectText="Edit" />
                                <asp:CommandField HeaderText="Delete" ShowDeleteButton="true" />
                            </Columns>
                                 <AlternatingRowStyle BackColor="PaleGoldenrod"/>
                            <FooterStyle BackColor="Tan" />
                            <HeaderStyle BackColor="Tan" Font-Bold="True" />
                            <PagerStyle BackColor="PaleGoldenrod" ForeColor="DarkSlateBlue" HorizontalAlign="Center" />
                            <SelectedRowStyle BackColor="DarkSlateBlue" ForeColor="GhostWhite" />
                            <SortedAscendingCellStyle BackColor="#FAFAE7" />
                            <SortedAscendingHeaderStyle BackColor="#DAC09E" />
                            <SortedDescendingCellStyle BackColor="#E1DB9C" />
                            <SortedDescendingHeaderStyle BackColor="#C2A47B" />
                        </asp:GridView>
                    </td>
                </tr>
        
       </table>

        </div>

        <div id="footer">
            All rights reserved..!!!
        </div>
            </div>
    
    </div>
    </form>
</body>
</html>
