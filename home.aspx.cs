﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void Order_Click(object sender, EventArgs e)
    {
        SqlConnection conn = null;
        SqlCommand cmd = null;
        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["payal"].ConnectionString);
            if (conn.State == ConnectionState.Closed)
            {
                conn.Open();
            }
            string query = "Insert into Customer(name,tableno)values(@name,@tableno)";
            cmd = new SqlCommand(query, conn);
            cmd.Parameters.AddWithValue("@name", Txtname.Text);
            cmd.Parameters.AddWithValue("@tableno", Txttable.Text);
            int no = cmd.ExecuteNonQuery();
            if(no==1)
            {
                Session["tableno"] = Txttable.Text;
                Session["name"] = Txtname.Text;
                Response.Redirect("order.aspx");
            }
        }
        catch(Exception ex)
        {
            Response.Write(ex.Message);
            throw;
        }

    }
}