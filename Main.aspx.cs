﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Main : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Session["name"] != null)
            {
                Lblname.Text = Session["name"].ToString();
            }
        }
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        SqlConnection conn;
        SqlCommand com;
        SqlDataReader dr;
        conn = new SqlConnection(ConfigurationManager.ConnectionStrings["payal"].ConnectionString);
        com = new SqlCommand("select * from Menu where Category = @Category", conn);
        conn.Open();
        com.Parameters.AddWithValue("@Category", ddlmenu.SelectedValue);
        dr = com.ExecuteReader();
        Grdmenu.DataSource = dr;
        Grdmenu.DataBind();
        dr.Close();
        conn.Close();
                                                    
    }

    protected void Grdmenu_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
}