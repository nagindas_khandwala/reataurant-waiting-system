﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class additem : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            LoadAllCategory();
        }
    }
    protected void btnadd_Click(object sender, EventArgs e)
    {
        SqlConnection conn = null;
        SqlCommand cmd = null;
        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["payal"].ConnectionString);
            if (conn.State == ConnectionState.Closed)
            {
                conn.Open();
            }
            string query = "Insert into Menu(menuname,price,category)values(@menuname,@price,@category)";
            cmd = new SqlCommand(query, conn);
            cmd.Parameters.AddWithValue("@menuname", txtmenuname.Text);
            cmd.Parameters.AddWithValue("@price", txtprice.Text);
            cmd.Parameters.AddWithValue("@category", txtcat.Text);
            int no = cmd.ExecuteNonQuery();
            if (no == 1)
            {
                Lblmsg.Text = "Inserted";
            }
            LoadAllCategory();

        }
        catch (Exception)
        {
            throw;
        }

    }
    private void LoadAllCategory()
    {
        SqlDataReader dr = null;
        SqlConnection conn = null;
        SqlCommand cmd = null;
        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["payal"].ConnectionString);
            if (conn.State == ConnectionState.Closed)
            {
                conn.Open();
            }
            string query = "Select * from Menu";
            cmd = new SqlCommand(query, conn);
            dr = cmd.ExecuteReader();
            GridView1.DataSource = dr;
            GridView1.DataBind();

        }
        catch (Exception)
        {
            throw;
        }

    }
    protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        SqlConnection conn = null;
        SqlCommand cmd = null;
        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["payal"].ConnectionString);
            int menuid = Convert.ToInt32(GridView1.DataKeys[e.RowIndex].Value);
            if (conn.State == ConnectionState.Closed)
            {
                conn.Open();
            }
            string query = "delete from Menu where menuid=@menuid";
            cmd = new SqlCommand(query, conn);
            cmd.Parameters.AddWithValue("@menuid", @menuid);
            cmd.ExecuteNonQuery();
            if (conn.State == ConnectionState.Open)
            {
                conn.Close();
            }
            LoadAllCategory();
        }
        catch (Exception)
        {

            throw;
        }
    }
}